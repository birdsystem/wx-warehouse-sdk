<?php

namespace WarehouseX\Warehouse;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getChangeLocationOrderCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ChangeLocationOrder[]',
        ],
        'postChangeLocationOrderCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\ChangeLocationOrder',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getChangeLocationOrderItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ChangeLocationOrder',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putChangeLocationOrderItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ChangeLocationOrder',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteChangeLocationOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchChangeLocationOrderItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ChangeLocationOrder',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientWarehouseCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ClientWarehouse[]',
        ],
        'postClientWarehouseCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\ClientWarehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ClientWarehouse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ClientWarehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientWarehouseItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchClientWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ClientWarehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentTransactionNoteCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransactionNote[]',
        ],
        'postLocationContentTransactionNoteCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransactionNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentTransactionNoteItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransactionNote',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putLocationContentTransactionNoteItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransactionNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteLocationContentTransactionNoteItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchLocationContentTransactionNoteItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransactionNote',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentTransactionCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransaction[]',
        ],
        'postLocationContentTransactionCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransaction',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentTransactionItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransaction',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putLocationContentTransactionItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransaction',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteLocationContentTransactionItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchLocationContentTransactionItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContentTransaction',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContent\\LocationContentOutput\\LocationContent\\Read[]',
        ],
        'postLocationContentCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\LocationContent\\LocationContentOutput\\LocationContent\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationContentItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContent\\LocationContentOutput\\LocationContent\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putLocationContentItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\LocationContent\\LocationContentOutput\\LocationContent\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteLocationContentItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Location\\Location\\Read[]',
        ],
        'postLocationCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\Location\\Location\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getLocationItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Location\\Location\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putLocationItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Location\\Location\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteLocationItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getWarehouseCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Warehouse[]',
        ],
        'postWarehouseCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\Warehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Warehouse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Warehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteWarehouseItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchWarehouseItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Warehouse',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getZoneTypeCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ZoneType\\ZoneType\\Read[]',
        ],
        'postZoneTypeCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\ZoneType\\ZoneType\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getZoneTypeItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ZoneType\\ZoneType\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putZoneTypeItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\ZoneType\\ZoneType\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteZoneTypeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getZoneCollection' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Zone\\Zone\\Read[]',
        ],
        'postZoneCollection' => [
            '201.' => 'WarehouseX\\Warehouse\\Model\\Zone\\Zone\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getZoneItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Zone\\Zone\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putZoneItem' => [
            '200.' => 'WarehouseX\\Warehouse\\Model\\Zone\\Zone\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteZoneItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
