<?php

namespace WarehouseX\Warehouse\Model\ZoneType\ZoneType;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ZoneType.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;
}
