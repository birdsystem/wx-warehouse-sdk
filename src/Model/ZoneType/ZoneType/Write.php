<?php

namespace WarehouseX\Warehouse\Model\ZoneType\ZoneType;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ZoneType.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;
}
