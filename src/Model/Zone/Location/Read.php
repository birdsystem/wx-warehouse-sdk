<?php

namespace WarehouseX\Warehouse\Model\Zone\Location;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Zone.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    public $warehouse = null;
}
