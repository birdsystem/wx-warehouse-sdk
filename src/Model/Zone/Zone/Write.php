<?php

namespace WarehouseX\Warehouse\Model\Zone\Zone;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Zone.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $warehouse = null;

    /**
     * @var string|null
     */
    public $zoneType = null;
}
