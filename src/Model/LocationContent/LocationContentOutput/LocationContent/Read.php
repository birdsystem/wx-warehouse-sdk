<?php

namespace WarehouseX\Warehouse\Model\LocationContent\LocationContentOutput\LocationContent;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LocationContent.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $contentType = null;

    /**
     * @var int
     */
    public $contentId = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var int
     */
    public $storageDays = null;

    public $createTime = null;

    public $updateTime = null;

    public $location = null;

    public $client = null;

    public $content = null;

    public $testField = null;
}
