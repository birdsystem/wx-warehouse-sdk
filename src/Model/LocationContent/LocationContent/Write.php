<?php

namespace WarehouseX\Warehouse\Model\LocationContent\LocationContent;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LocationContent.
 */
class Write extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $contentType = null;

    /**
     * @var int
     */
    public $contentId = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var string|null
     */
    public $location = null;
}
