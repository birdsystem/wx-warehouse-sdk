<?php

namespace WarehouseX\Warehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientWarehouse.
 */
class ClientWarehouse extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $warehouse = null;
}
