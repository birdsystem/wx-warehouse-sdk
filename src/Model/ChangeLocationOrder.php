<?php

namespace WarehouseX\Warehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ChangeLocationOrder.
 */
class ChangeLocationOrder extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $contentId = null;

    /**
     * @var string
     */
    public $contentType = 'CARTON';

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $userIdAdmin = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $newLocation = null;

    /**
     * @var string|null
     */
    public $oldLocation = null;

    /**
     * @var string|null
     */
    public $warehouse = null;
}
