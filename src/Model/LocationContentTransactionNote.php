<?php

namespace WarehouseX\Warehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LocationContentTransactionNote.
 */
class LocationContentTransactionNote extends AbstractModel
{
    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $locationContentTransaction = null;
}
