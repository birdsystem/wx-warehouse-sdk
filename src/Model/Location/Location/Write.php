<?php

namespace WarehouseX\Warehouse\Model\Location\Location;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Location.
 */
class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $status = 'EMPTY';

    /**
     * @var string|null
     */
    public $zone = null;
}
