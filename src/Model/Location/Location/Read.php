<?php

namespace WarehouseX\Warehouse\Model\Location\Location;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Location.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string
     */
    public $status = 'EMPTY';

    public $zone = null;

    /**
     * @var string
     */
    public $createTime = null;
}
