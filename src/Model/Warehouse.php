<?php

namespace WarehouseX\Warehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Warehouse.
 */
class Warehouse extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $address = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';
}
