<?php

namespace WarehouseX\Warehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * LocationContentTransaction.
 */
class LocationContentTransaction extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $contentType = null;

    /**
     * @var int
     */
    public $contentId = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var int
     */
    public $recordId = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $location = null;
}
