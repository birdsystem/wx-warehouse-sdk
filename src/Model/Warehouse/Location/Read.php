<?php

namespace WarehouseX\Warehouse\Model\Warehouse\Location;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Warehouse.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $reference = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';
}
