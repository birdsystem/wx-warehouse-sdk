<?php

namespace WarehouseX\Warehouse;

use OpenAPI\Runtime\ResponseHandler\Allow404ResponseStatusHandler;
use OpenAPI\Runtime\ResponseHandler\GenericResponseHandler;
use OpenAPI\Runtime\ResponseHandler\JsonResponseHandler;
use OpenAPI\Runtime\ResponseHandlerStack\ResponseHandlerStack as BaseClass;

class ResponseHandlerStack extends BaseClass
{
    public function __construct()
    {
        $handlers[] = new GenericResponseHandler();
        $jsonResponsHandler = new JsonResponseHandler();
        $jsonResponsHandler->setResponseTypes(new ResponseTypes());
        $handlers[] = $jsonResponsHandler;
        $handlers[] = new Allow404ResponseStatusHandler();
        parent::__construct($handlers);
    }
}
