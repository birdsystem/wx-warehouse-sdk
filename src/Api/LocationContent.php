<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\LocationContent\LocationContent\Put as Put;
use WarehouseX\Warehouse\Model\LocationContent\LocationContent\Write as Write;
use WarehouseX\Warehouse\Model\LocationContent\LocationContentOutput\LocationContent\Read as Read;

class LocationContent extends AbstractAPI
{
    /**
     * Retrieves the collection of LocationContent resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'contentType'	string
     *                       'contentType[]'	array
     *                       'contentId'	integer
     *                       'contentId[]'	array
     *                       'quantity'	integer
     *                       'quantity[]'	array
     *                       'location'	string
     *                       'location[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'quantity[between]'	string
     *                       'quantity[gt]'	string
     *                       'quantity[gte]'	string
     *                       'quantity[lt]'	string
     *                       'quantity[lte]'	string
     *                       'storageDays[between]'	string
     *                       'storageDays[gt]'	string
     *                       'storageDays[gte]'	string
     *                       'storageDays[lt]'	string
     *                       'storageDays[lte]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getLocationContentCollection',
        'GET',
        'api/warehouse/location_contents',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a LocationContent resource.
     *
     * @param Write $Model The new LocationContent resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postLocationContentCollection',
        'POST',
        'api/warehouse/location_contents',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a LocationContent resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getLocationContentItem',
        'GET',
        "api/warehouse/location_contents/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the LocationContent resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated LocationContent resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putLocationContentItem',
        'PUT',
        "api/warehouse/location_contents/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the LocationContent resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteLocationContentItem',
        'DELETE',
        "api/warehouse/location_contents/$id",
        null,
        [],
        []
        );
    }
}
