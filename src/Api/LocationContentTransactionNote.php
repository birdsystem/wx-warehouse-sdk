<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\LocationContentTransactionNote as LocationContentTransactionNoteModel;

class LocationContentTransactionNote extends AbstractAPI
{
    /**
     * Retrieves the collection of LocationContentTransactionNote resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return LocationContentTransactionNoteModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getLocationContentTransactionNoteCollection',
        'GET',
        'api/warehouse/location_content_transaction_notes',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a LocationContentTransactionNote resource.
     *
     * @param LocationContentTransactionNoteModel $Model The new
     *                                                   LocationContentTransactionNote resource
     *
     * @return LocationContentTransactionNoteModel
     */
    public function postCollection(LocationContentTransactionNoteModel $Model): LocationContentTransactionNoteModel
    {
        return $this->request(
        'postLocationContentTransactionNoteCollection',
        'POST',
        'api/warehouse/location_content_transaction_notes',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a LocationContentTransactionNote resource.
     *
     * @param string $locationContentTransaction Resource identifier
     *
     * @return LocationContentTransactionNoteModel|null
     */
    public function getItem(string $locationContentTransaction): ?LocationContentTransactionNoteModel
    {
        return $this->request(
        'getLocationContentTransactionNoteItem',
        'GET',
        "api/warehouse/location_content_transaction_notes/$locationContentTransaction",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the LocationContentTransactionNote resource.
     *
     * @param string                              $locationContentTransaction Resource identifier
     * @param LocationContentTransactionNoteModel $Model                      The updated
     *                                                                        LocationContentTransactionNote resource
     *
     * @return LocationContentTransactionNoteModel
     */
    public function putItem(string $locationContentTransaction, LocationContentTransactionNoteModel $Model): LocationContentTransactionNoteModel
    {
        return $this->request(
        'putLocationContentTransactionNoteItem',
        'PUT',
        "api/warehouse/location_content_transaction_notes/$locationContentTransaction",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the LocationContentTransactionNote resource.
     *
     * @param string $locationContentTransaction Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $locationContentTransaction): mixed
    {
        return $this->request(
        'deleteLocationContentTransactionNoteItem',
        'DELETE',
        "api/warehouse/location_content_transaction_notes/$locationContentTransaction",
        null,
        [],
        []
        );
    }

    /**
     * Updates the LocationContentTransactionNote resource.
     *
     * @param string $locationContentTransaction Resource identifier
     *
     * @return LocationContentTransactionNoteModel
     */
    public function patchItem(string $locationContentTransaction): LocationContentTransactionNoteModel
    {
        return $this->request(
        'patchLocationContentTransactionNoteItem',
        'PATCH',
        "api/warehouse/location_content_transaction_notes/$locationContentTransaction",
        null,
        [],
        []
        );
    }
}
