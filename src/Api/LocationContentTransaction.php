<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\LocationContentTransaction as LocationContentTransactionModel;

class LocationContentTransaction extends AbstractAPI
{
    /**
     * Retrieves the collection of LocationContentTransaction resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'contentType'	string
     *                       'contentType[]'	array
     *                       'contentId'	integer
     *                       'contentId[]'	array
     *                       'recordId'	integer
     *                       'recordId[]'	array
     *                       'type'	string
     *                       'type[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'location'	string
     *                       'location[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'quantity[between]'	string
     *                       'quantity[gt]'	string
     *                       'quantity[gte]'	string
     *                       'quantity[lt]'	string
     *                       'quantity[lte]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return LocationContentTransactionModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getLocationContentTransactionCollection',
        'GET',
        'api/warehouse/location_content_transactions',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a LocationContentTransaction resource.
     *
     * @param LocationContentTransactionModel $Model The new LocationContentTransaction
     *                                               resource
     *
     * @return LocationContentTransactionModel
     */
    public function postCollection(LocationContentTransactionModel $Model): LocationContentTransactionModel
    {
        return $this->request(
        'postLocationContentTransactionCollection',
        'POST',
        'api/warehouse/location_content_transactions',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a LocationContentTransaction resource.
     *
     * @param string $id Resource identifier
     *
     * @return LocationContentTransactionModel|null
     */
    public function getItem(string $id): ?LocationContentTransactionModel
    {
        return $this->request(
        'getLocationContentTransactionItem',
        'GET',
        "api/warehouse/location_content_transactions/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the LocationContentTransaction resource.
     *
     * @param string                          $id    Resource identifier
     * @param LocationContentTransactionModel $Model The updated
     *                                               LocationContentTransaction resource
     *
     * @return LocationContentTransactionModel
     */
    public function putItem(string $id, LocationContentTransactionModel $Model): LocationContentTransactionModel
    {
        return $this->request(
        'putLocationContentTransactionItem',
        'PUT',
        "api/warehouse/location_content_transactions/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the LocationContentTransaction resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteLocationContentTransactionItem',
        'DELETE',
        "api/warehouse/location_content_transactions/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the LocationContentTransaction resource.
     *
     * @param string $id Resource identifier
     *
     * @return LocationContentTransactionModel
     */
    public function patchItem(string $id): LocationContentTransactionModel
    {
        return $this->request(
        'patchLocationContentTransactionItem',
        'PATCH',
        "api/warehouse/location_content_transactions/$id",
        null,
        [],
        []
        );
    }
}
