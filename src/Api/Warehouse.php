<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\Warehouse as WarehouseModel;

class Warehouse extends AbstractAPI
{
    /**
     * Retrieves the collection of Warehouse resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'name'	string
     *                       'address'	string
     *                       'reference'	string
     *                       'note'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return WarehouseModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getWarehouseCollection',
        'GET',
        'api/warehouse/warehouses',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Warehouse resource.
     *
     * @param WarehouseModel $Model The new Warehouse resource
     *
     * @return WarehouseModel
     */
    public function postCollection(WarehouseModel $Model): WarehouseModel
    {
        return $this->request(
        'postWarehouseCollection',
        'POST',
        'api/warehouse/warehouses',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Warehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return WarehouseModel|null
     */
    public function getItem(string $id): ?WarehouseModel
    {
        return $this->request(
        'getWarehouseItem',
        'GET',
        "api/warehouse/warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Warehouse resource.
     *
     * @param string         $id    Resource identifier
     * @param WarehouseModel $Model The updated Warehouse resource
     *
     * @return WarehouseModel
     */
    public function putItem(string $id, WarehouseModel $Model): WarehouseModel
    {
        return $this->request(
        'putWarehouseItem',
        'PUT',
        "api/warehouse/warehouses/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Warehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteWarehouseItem',
        'DELETE',
        "api/warehouse/warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the Warehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return WarehouseModel
     */
    public function patchItem(string $id): WarehouseModel
    {
        return $this->request(
        'patchWarehouseItem',
        'PATCH',
        "api/warehouse/warehouses/$id",
        null,
        [],
        []
        );
    }
}
