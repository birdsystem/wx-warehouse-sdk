<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\ZoneType\Client\Edit as Edit;
use WarehouseX\Warehouse\Model\ZoneType\ZoneType\Read as Read;
use WarehouseX\Warehouse\Model\ZoneType\ZoneType\Write as Write;

class ZoneType extends AbstractAPI
{
    /**
     * Retrieves the collection of ZoneType resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getZoneTypeCollection',
        'GET',
        'api/warehouse/zone_types',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ZoneType resource.
     *
     * @param Write $Model The new ZoneType resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postZoneTypeCollection',
        'POST',
        'api/warehouse/zone_types',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ZoneType resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getZoneTypeItem',
        'GET',
        "api/warehouse/zone_types/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ZoneType resource.
     *
     * @param string $id    Resource identifier
     * @param Edit   $Model The updated ZoneType resource
     *
     * @return Read
     */
    public function putItem(string $id, Edit $Model): Read
    {
        return $this->request(
        'putZoneTypeItem',
        'PUT',
        "api/warehouse/zone_types/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ZoneType resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteZoneTypeItem',
        'DELETE',
        "api/warehouse/zone_types/$id",
        null,
        [],
        []
        );
    }
}
