<?php

namespace WarehouseX\Warehouse\Api;

use Psr\Http\Client\ClientInterface as BaseClass;

interface HttpClientInterface extends BaseClass
{
}
