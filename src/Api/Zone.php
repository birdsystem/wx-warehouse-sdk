<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\Zone\Location\Put as Put;
use WarehouseX\Warehouse\Model\Zone\Zone\Read as Read;
use WarehouseX\Warehouse\Model\Zone\Zone\Write as Write;

class Zone extends AbstractAPI
{
    /**
     * Retrieves the collection of Zone resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'reference[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *                       'warehouse'	string
     *                       'warehouse[]'	array
     *                       'zoneType'	string
     *                       'zoneType[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getZoneCollection',
        'GET',
        'api/warehouse/zones',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Zone resource.
     *
     * @param Write $Model The new Zone resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postZoneCollection',
        'POST',
        'api/warehouse/zones',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Zone resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getZoneItem',
        'GET',
        "api/warehouse/zones/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Zone resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated Zone resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putZoneItem',
        'PUT',
        "api/warehouse/zones/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Zone resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteZoneItem',
        'DELETE',
        "api/warehouse/zones/$id",
        null,
        [],
        []
        );
    }
}
