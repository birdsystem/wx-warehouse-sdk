<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\ClientWarehouse as ClientWarehouseModel;

class ClientWarehouse extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientWarehouse resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'warehouse'	string
     *                       'warehouse[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return ClientWarehouseModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientWarehouseCollection',
        'GET',
        'api/warehouse/client_warehouses',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientWarehouse resource.
     *
     * @param ClientWarehouseModel $Model The new ClientWarehouse resource
     *
     * @return ClientWarehouseModel
     */
    public function postCollection(ClientWarehouseModel $Model): ClientWarehouseModel
    {
        return $this->request(
        'postClientWarehouseCollection',
        'POST',
        'api/warehouse/client_warehouses',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return ClientWarehouseModel|null
     */
    public function getItem(string $id): ?ClientWarehouseModel
    {
        return $this->request(
        'getClientWarehouseItem',
        'GET',
        "api/warehouse/client_warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientWarehouse resource.
     *
     * @param string               $id    Resource identifier
     * @param ClientWarehouseModel $Model The updated ClientWarehouse resource
     *
     * @return ClientWarehouseModel
     */
    public function putItem(string $id, ClientWarehouseModel $Model): ClientWarehouseModel
    {
        return $this->request(
        'putClientWarehouseItem',
        'PUT',
        "api/warehouse/client_warehouses/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientWarehouseItem',
        'DELETE',
        "api/warehouse/client_warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the ClientWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return ClientWarehouseModel
     */
    public function patchItem(string $id): ClientWarehouseModel
    {
        return $this->request(
        'patchClientWarehouseItem',
        'PATCH',
        "api/warehouse/client_warehouses/$id",
        null,
        [],
        []
        );
    }
}
