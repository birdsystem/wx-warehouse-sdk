<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\ChangeLocationOrder as ChangeLocationOrderModel;

class ChangeLocationOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of ChangeLocationOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'warehouse'	string
     *                       'warehouse[]'	array
     *                       'quantity'	integer
     *                       'quantity[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'userIdAdmin'	integer
     *                       'userIdAdmin[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return ChangeLocationOrderModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getChangeLocationOrderCollection',
        'GET',
        'api/warehouse/change_location_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ChangeLocationOrder resource.
     *
     * @param ChangeLocationOrderModel $Model The new ChangeLocationOrder resource
     *
     * @return ChangeLocationOrderModel
     */
    public function postCollection(ChangeLocationOrderModel $Model): ChangeLocationOrderModel
    {
        return $this->request(
        'postChangeLocationOrderCollection',
        'POST',
        'api/warehouse/change_location_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ChangeLocationOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return ChangeLocationOrderModel|null
     */
    public function getItem(string $id): ?ChangeLocationOrderModel
    {
        return $this->request(
        'getChangeLocationOrderItem',
        'GET',
        "api/warehouse/change_location_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ChangeLocationOrder resource.
     *
     * @param string                   $id    Resource identifier
     * @param ChangeLocationOrderModel $Model The updated ChangeLocationOrder resource
     *
     * @return ChangeLocationOrderModel
     */
    public function putItem(string $id, ChangeLocationOrderModel $Model): ChangeLocationOrderModel
    {
        return $this->request(
        'putChangeLocationOrderItem',
        'PUT',
        "api/warehouse/change_location_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ChangeLocationOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteChangeLocationOrderItem',
        'DELETE',
        "api/warehouse/change_location_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the ChangeLocationOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return ChangeLocationOrderModel
     */
    public function patchItem(string $id): ChangeLocationOrderModel
    {
        return $this->request(
        'patchChangeLocationOrderItem',
        'PATCH',
        "api/warehouse/change_location_orders/$id",
        null,
        [],
        []
        );
    }
}
