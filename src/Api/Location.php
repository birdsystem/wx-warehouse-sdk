<?php

namespace WarehouseX\Warehouse\Api;

use WarehouseX\Warehouse\Model\Location\Location\Put as Put;
use WarehouseX\Warehouse\Model\Location\Location\Read as Read;
use WarehouseX\Warehouse\Model\Location\Location\Write as Write;

class Location extends AbstractAPI
{
    /**
     * Retrieves the collection of Location resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'reference'	string
     *                       'reference[]'	array
     *                       'zone'	string
     *                       'zone[]'	array
     *                       'zone.warehouse'	string
     *                       'zone.warehouse[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getLocationCollection',
        'GET',
        'api/warehouse/locations',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Location resource.
     *
     * @param Write $Model The new Location resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postLocationCollection',
        'POST',
        'api/warehouse/locations',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Location resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getLocationItem',
        'GET',
        "api/warehouse/locations/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Location resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated Location resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putLocationItem',
        'PUT',
        "api/warehouse/locations/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Location resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteLocationItem',
        'DELETE',
        "api/warehouse/locations/$id",
        null,
        [],
        []
        );
    }
}
