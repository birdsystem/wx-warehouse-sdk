# Official Warehouse-X.com API SDK (PHP) - warehouse module

Code generated from Warehouse-X.com's OpenAPI file
using [allansun/openapi-code-generator](https://github.com/allansun/openapi-code-generator).

Generated code is well self-documented with proper PHPDoc annotations.

Please refer to [Warehouse-X.com's documentation](http://www.warehouse-x.com/api/warehouse/docs) for detailed API 
behaviour explanation.

## Installation

```shell
composer require warehouse-x/warehouse-sdk
```

You will also need a [PSR-18 compatible client](https://www.php-fig.org/psr/psr-18/) see
[https://docs.php-http.org/en/latest/clients.html](https://docs.php-http.org/en/latest/clients.html)

So either use Guzzle (or any other PSR-18 compatible clients)

```shell
composer require php-http/guzzle7-adapter
```

## Versioning

This project matches Warehouse-X's API versioning.

Should you found a matching version not being available. Please contact the author to generate against correct version.

## Usage

First you need to create a PSR-18 Then in your business logic you can call API operations directly

```php
<?php
use WarehouseX\ClOrder\Api\Inboundorder;
$httpClient = new \GuzzleHttp\Client([
    'base_uri' => 'https://www.warehouse-x.com/',
    'headers'=>[
        'Authorization'=> 'Bearer <accessToken>'
    ]
]);
$api = new Inboundorder($httpClient);

$orders = $api->getInboundOrderCollection();

```

## Author

* [Allan Sun](https://github.com/allansun) - *Initial work*
